<?php
/**
 * @file
 * phoenix_card.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function phoenix_card_eck_bundle_info() {
  $items = array(
    'compro_component_card' => array(
      'machine_name' => 'compro_component_card',
      'entity_type' => 'compro_component',
      'name' => 'card',
      'label' => 'Card',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'language' => 0,
        ),
      ),
    ),
  );
  return $items;
}
